<?php

namespace Poject\Strategy\Model;

class Feriados implements Strategy {
    private $mensagemEspecial;

    public function __construct(string $mensagemEspecial) {
        $this->mensagemEspecial = $mensagemEspecial;
    }

    public function format($mensagem)
    {
        return $this->mensagemEspecial.$mensagem;
    }
}