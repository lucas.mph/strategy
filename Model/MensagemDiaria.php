<?php

namespace Poject\Strategy\Model;

class MensagemDiaria {
    private $mensagem;
    private Strategy $strategy;

    public function __construct(string $mensagem, Strategy $strategy) {
        $this->mensagem = $mensagem;
        $this->strategy = $strategy;
    }

    public function setStrategy(Strategy $strategy) {
        $this->strategy = $strategy;
    }

    public function getMessage() {
        return $this->strategy->format($this->mensagem);
    }
}