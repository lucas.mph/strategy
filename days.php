<?php 

class MensagemDiaria {
    private $mensagem;

    public function __construct(string $mensagem) {
        $this->mensagem = $mensagem;
    }

    public function getMessage() {
        return $this->mensagem;
    }
}

interface Strategy {
    public function format($mansagem);
}

class DiasdaSemana implements Strategy {
    public function format($mensagem)
    {
        return $mensagem;
    }
}

class Feriados implements Strategy {
    public function format($mensagem)
    {
        return 'saudacao '. $mensagem;
    }
}